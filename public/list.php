<?php

$log = file_get_contents( dirname(__FILE__).'/../fixtures/certificates.log');

if (false !== strpos($log, 'Found the following certs:'))
{
    preg_match_all(
        '/(Certificate\\s+Name:\\s+(?<name>.+))\\n\\s+'.
        '(Domains:\\s+(?P<domains>.+))\\n\\s+'.
        '(Expiry\\s+Date:\\s+(?<expire_date>.+))\\s+\\(VALID:\\s+(?P<valid_days>\\d+)\\s+days\\)\\n\\s+'.
        '(Certificate\\s+Path:\\s+(?P<cert_path>.+))\\n\\s+'.
        '(Private\\s+Key\\s+Path:\\s+(?P<key_path>.+))/uim', $log, $certs, PREG_SET_ORDER);


    $certs = array_map(function($cert){
        $cert['expire_date'] = date('Y-m-d H:i:s', strtotime($cert['expire_date']));
        $cert['domains'] = explode(' ', $cert['domains']);

        return $cert;
    }, $certs);

    echo sprintf("Found %d certs\n", count($certs));

    foreach ($certs as $cert)
    {
        echo sprintf("Cert: %s\nDomains: %s\n", $cert['name'], implode(', ', $cert['domains']));
        echo sprintf("Expire: %s\n", $cert['expire_date']);

        echo "-------------------------------------------------------------\n";
    }

} else {
    echo "Certificates not found\n";
}
