#!/bin/bash

CERT = $1
DOMAINS = $2

certbot --expand --cert-name $CERT --allow-subset-of-names -d $DOMAINS
done