#!/bin/bash

for CERT in "$@"
do
 certbot renew --cert-name $CERT --renew-with-new-domains --allow-subset-of-names
done